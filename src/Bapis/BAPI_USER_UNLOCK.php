<?php


namespace LumenInnovation\SAP\Bapis;


use LumenInnovation\SAP\SAP;
use ReflectionClass;

class BAPI_USER_UNLOCK
{
    protected $bapi;

    public function __construct(SAP $sap)
    {
        $this->bapi = $sap->fm((new ReflectionClass($this))->getShortName());
    }

    public function unlock($username)
    {
        return $this->bapi->addParameter('USERNAME', $username)->invoke();
    }
}