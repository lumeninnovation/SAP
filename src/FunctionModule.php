<?php


namespace LumenInnovation\SAP;


use LumenInnovation\SAP\Exceptions\FunctionModuleParameterBindException;

class FunctionModule
{
    protected $sap;

    protected $name;

    protected $fm;

    protected $attributes;

    protected $parameters = [];

    public function __construct(SAP $sap, $name)
    {
        $this->sap = $sap;

        $this->name = $name;

        $this->init();
    }

    protected function init()
    {
        $this->fm = $this->sap->getHandle()->getFunction($this->name);

        $this->attributes = collect(
            json_decode(
                json_encode($this->fm),
                true
            )
        )->except('name');
    }

    public function invoke()
    {
        return $this->fm->invoke($this->parameters);
    }

    public function addParameter($param, $value)
    {
        $this->hasAttribute($param);

        $this->parameters[$param] = $value;

        return $this;
    }

    protected function hasAttribute($value)
    {
        if(!$this->attributes->has($value))
            throw new FunctionModuleParameterBindException('Function module ' . $this->name . ' does not contain the attribute ' . $value);
    }
}