<?php


namespace LumenInnovation\SAP;


use Illuminate\Foundation\Application as LaravelApplication;
use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Application as LumenApplication;

class SapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $source = realpath($raw = __DIR__ . '/../config/sap.php') ?: $raw;

        if ($this->app instanceof LaravelApplication && $this->app->runningInConsole()) {
            //Loads Config
            $this->publishes([$source => config_path('sap.php')]);
            //Load Migrations
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        } elseif ($this->app instanceof LumenApplication) {
            $this->app->configure('sap');
        }


        if ($this->app instanceof LaravelApplication && !$this->app->configurationIsCached()) {
            $this->mergeConfigFrom($source, 'sap');
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('LumenInnovation\SAP\SAP', function ($app) {
            return new SAP();
        });

        $this->app->alias('LumenInnovation\SAP\SAP', 'SAP');
    }
}