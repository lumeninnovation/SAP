<?php


namespace LumenInnovation\SAP;

use SAPNWRFC\Connection as SapConnection;

class SAP
{
    protected $config;

    protected $handle;

    public function __construct($connection = null)
    {
        $con = $connection ? $connection : config('sap.default.connection');

        $this->config = config('sap.connections.' . $con);
    }

    public function connect($username, $password, $connection = null)
    {
        if($connection) {
            $this->config = config('sap.connections.' . $connection);
        }

        try {
            $this->handle = new SapConnection(array_merge($this->config, [
                'user' => $username,
                'passwd' => $password
            ]));
        } catch(\Exception $exception) {
            return $this;
        }

        return $this;
    }

    public function ping()
    {
        return $this->handle ? $this->handle->ping() : false;
    }

    public function getHandle()
    {
        return $this->handle;
    }

    public function fm($name)
    {
        return new FunctionModule($this, $name);
    }
}